FROM node:slim as build-stage
RUN npm install -g serve vue
EXPOSE 5002
ENV NODE_ENV production
WORKDIR /webapp
ADD package.json .
RUN yarn install --network-timeout=100000
ADD . .
RUN if [ -d "dist/" ]; then rm -rf dist; fi
RUN yarn build
#CMD serve -p 5002 -s dist

FROM nginx:1.13.12-alpine as production-stage
COPY --from=build-stage /webapp/dist /usr/share/nginx/html
COPY ./nginx/conf.d /etc/nginx/conf.d
EXPOSE 80
CMD ["nginx","-g","daemon off;"]
