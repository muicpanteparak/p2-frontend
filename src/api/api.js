import Axios from 'axios'

const URL = process.env.NODE_ENV === 'production' ? '/api' : "http://localhost:9001'"
// console.log(URL)
const play = Axios.create({
  // baseURL: 'api/',
  baseURL: URL,
  withCredentials: true,
  headers: {
    'Content-Type': 'application/json'
    // 'Hosts': 'localhost:9001'
  }
})

export default play
