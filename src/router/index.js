import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Tracker from '@/components/Tracker'
Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: Home
    },
    {
      path: '/track/:id',
      component: Tracker
    }
  ]
})
